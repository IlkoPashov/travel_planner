package com.example.travel_planner.exceptions;

public class EntityNotFoundException extends RuntimeException {
    private String errorType;

    public EntityNotFoundException() {
    }


    public EntityNotFoundException(String type) {
        super((String.format("%s does not exist", type)));
        this.errorType = type;
    }

    public String getErrorType() {
        return errorType;
    }

}
