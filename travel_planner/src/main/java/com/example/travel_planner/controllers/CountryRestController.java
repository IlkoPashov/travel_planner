package com.example.travel_planner.controllers;

import com.example.travel_planner.exceptions.EntityNotFoundException;
import com.example.travel_planner.services.contracts.CountryService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.json.JSONException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Map;

@RestController
@RequestMapping("/api/country")
public class CountryRestController {

    private final CountryService countryService;



    public CountryRestController(CountryService countryService) {
        this.countryService = countryService;
    }


    @GetMapping("/currencies")
    public ResponseEntity<Map<String, Double>> convertCurrencies(
            @RequestParam("countryName") String countryName,
            @RequestParam("currency") String currency
    ) {
        try {
            Map<String, Double> convertedCurrencies = countryService.convertCurrencies(countryName, currency);
            return ResponseEntity.ok(convertedCurrencies);
        } catch (JSONException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    @GetMapping("/information")
    public String getInformation(@RequestParam("countryName") String countryName,
                                 @RequestParam("budgedPerCountry") double budgedPerCountry,
                                 @RequestParam("totalBudged") double totalBudged,
                                 @RequestParam("currency") String currency) throws JSONException, JsonProcessingException {

        try{
            return countryService.calculateCountrVisit(countryName,budgedPerCountry,totalBudged,currency);
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

    }

}


