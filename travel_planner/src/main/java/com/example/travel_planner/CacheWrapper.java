package com.example.travel_planner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
public class CacheWrapper {

    private final RedisTemplate<String, Object> redisTemplate;
    public final String COUNTRY_NAME_SUB_KEY = "country:";

    @Autowired
    public CacheWrapper(RedisTemplate<String, Object> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    public void setValue(String key, Object value) {
        redisTemplate.opsForValue().set(key, value);
    }

    public void setValue(String key, Object value, int expirationSeconds) {
        redisTemplate.opsForValue().set(key, value);
        redisTemplate.expire(key, expirationSeconds, TimeUnit.SECONDS);

    }

    public Object getValue(String key) {
        return redisTemplate.opsForValue().get(key);
    }

    public boolean keyExist(String key) {
        return redisTemplate.hasKey(key);
    }

    public void clearAllKeys() {
        redisTemplate.execute((RedisCallback<Object>) connection -> {
            connection.flushAll();
            return null;
        });
    }
}