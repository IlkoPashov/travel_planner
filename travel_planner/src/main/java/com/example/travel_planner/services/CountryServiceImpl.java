package com.example.travel_planner.services;

import com.example.travel_planner.exceptions.EntityNotFoundException;
import com.example.travel_planner.models.Country;
import com.example.travel_planner.repositories.contracts.CountryRepository;
import com.example.travel_planner.services.contracts.CountryService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class CountryServiceImpl implements CountryService {
    private final CountryRepository countryRepository;

    private final NeighboringCountriesApiClient neighboringCountriesApiClient;


    @Autowired
    public CountryServiceImpl(CountryRepository countryRepository, NeighboringCountriesApiClient neighboringCountriesApiClient) {
        this.countryRepository = countryRepository;
        this.neighboringCountriesApiClient = neighboringCountriesApiClient;
    }


    @Override
    public Country getCountryByName(String name) throws JsonProcessingException {
        return countryRepository.getCountryByName(name);
    }


    @Override
    public String calculateCountrVisit(String countryName, double budgedPerCountry, double totalBudged, String currency) throws JSONException, JsonProcessingException {
        Country country = getCountryByName(countryName);
        validateInput(countryName);

        List<String> neighbors = countryRepository.getNeighborCountries(country.getName());

        double budgedPerVisit = neighbors.size() * budgedPerCountry;

        int totalVisits = (int) Math.floor(totalBudged / budgedPerVisit);
        double leftOver = totalBudged - (budgedPerVisit * totalVisits);


        Map<String, Double> convertedCurrencies = convertCurrencies(countryName, currency);

        StringBuilder result = new StringBuilder();

        result.append(countryName).append(" has ").append(neighbors.size()).append(" neighbor countries (");
        for (int i = 0; i < neighbors.size(); i++) {
            result.append(neighbors.get(i));
            if (i != neighbors.size() - 1) {
                result.append(", ");
            }
        }
        result.append(") and Angel can travel around them ").append(totalVisits).append(" times. He will have ")
                .append(leftOver).append(String.format(" %s leftover.",currency)).append(System.lineSeparator());

        for (Map.Entry<String, Double> entry : convertedCurrencies.entrySet()) {
            String key = entry.getKey();
            Double value = entry.getValue() * totalVisits * budgedPerCountry;

            result.append(key).append(": ").append(" he will need to buy ").append(String.format("%.2f", value)).append("\n");
        }
        result.deleteCharAt(result.length() - 1);  // Remove the trailing comma

        return result.toString();


    }

    @Override
    public Map<String, Double> convertCurrencies(String countryName, String currency) throws JSONException, JsonProcessingException {
        List<String> neighborCurrencyCodes = countryRepository.getNeighborCurrencyCodes(countryName);
        JSONObject exchangeRates = neighboringCountriesApiClient.getNeighborsCurrenciesCodes(currency);
        Map<String, Double> convertedCurrencies = new HashMap<>();
        for (String neighborCurrencyCode : neighborCurrencyCodes) {
            if (exchangeRates.has(neighborCurrencyCode)) {
                JSONObject exchangeRate = exchangeRates.getJSONObject(neighborCurrencyCode);
                double rate = exchangeRate.getDouble("rate");
                convertedCurrencies.put(neighborCurrencyCode, rate);
            }
        }
        return convertedCurrencies;
    }

    private void validateInput(String countryName) {
        boolean countryExists;

        try {
            Country country = countryRepository.getCountryByName(countryName);
            countryExists = (country != null);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

        if (!countryExists) {
            throw new EntityNotFoundException(countryName);
        }

    }
}


