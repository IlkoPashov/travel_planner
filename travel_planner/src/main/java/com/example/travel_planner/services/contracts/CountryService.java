package com.example.travel_planner.services.contracts;

import com.example.travel_planner.models.Country;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.json.JSONException;

import java.util.List;
import java.util.Map;

public interface CountryService {


    Country getCountryByName(String name) throws JsonProcessingException;

    String calculateCountrVisit(String testName, double budgedPerCountry, double totalBudged, String currency) throws JSONException, JsonProcessingException;


    Map<String, Double> convertCurrencies(String countryName, String currency) throws JSONException, JsonProcessingException;


}
