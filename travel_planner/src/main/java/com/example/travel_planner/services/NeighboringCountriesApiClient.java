package com.example.travel_planner.services;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class NeighboringCountriesApiClient {
    private static final String API_BASE_URL = "https://restcountries.com/v3.1/";

    private static final String CURRENCY_BASE_URL = "https://www.floatrates.com/daily/";

    private final OkHttpClient client;
    private final ObjectMapper objectMapper;

    @Autowired
    public NeighboringCountriesApiClient() {
        client = new OkHttpClient();
        objectMapper = new ObjectMapper();
    }

    public JSONObject getNeighborsCurrenciesCodes(String currencyCode) {
        String url = CURRENCY_BASE_URL + currencyCode + ".json";
        Request request = new Request.Builder()
                .url(url)
                .build();
        try (Response response = client.newCall(request).execute()) {
            if (response.isSuccessful()) {
                String responseBody = response.body().string();
                JSONObject jsonObject = new JSONObject(responseBody);
                return jsonObject;

            }
        } catch (JSONException | IOException e) {
            throw new RuntimeException(e);
        }
        return null;
    }

    public List<String> getNeighboringCountries(String countryName) throws IOException {
        String url = API_BASE_URL + "name/" + countryName;
        System.out.println(url);
        Request request = new Request.Builder()
                .url(url)
                .build();

        try (Response response = client.newCall(request).execute()) {
            if (response.isSuccessful()) {
                String responseBody = response.body().string();
                JSONArray jsonArray = new JSONArray(responseBody);

                if (jsonArray.length() > 0) {
                    JSONObject countryObject = jsonArray.getJSONObject(0);
                    JSONArray bordersArray = countryObject.getJSONArray("borders");
                    JSONArray currencyArray = countryObject.getJSONArray("currencies");
                    System.out.println(countryObject);
                    List<String> borders = new ArrayList<>();
                    for (int i = 0; i < bordersArray.length(); i++) {
                        borders.add(bordersArray.getString(i));
                    }

                    return borders;
                }
            }
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        return null;
    }

}
