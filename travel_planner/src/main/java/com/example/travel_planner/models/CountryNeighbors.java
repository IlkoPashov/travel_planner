package com.example.travel_planner.models;

import jakarta.persistence.*;

import java.util.Objects;

@Entity
@Table(name= "country_neighbors")
public class CountryNeighbors {


    @Id
    private int id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "country_id")
    private Country country;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "neighbor_id")
    private Country neighbor;


    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Country getNeighbor() {
        return neighbor;
    }

    public void setNeighbor(Country neighbor) {
        this.neighbor = neighbor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CountryNeighbors that = (CountryNeighbors) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
