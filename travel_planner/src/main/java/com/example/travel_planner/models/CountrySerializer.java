package com.example.travel_planner.models;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;


@Component
public class CountrySerializer {

    private final ObjectMapper objectMapper;

    public CountrySerializer(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    public String serializeCountry(Country country) throws JsonProcessingException {
        return objectMapper.writeValueAsString(country);
    }

    public Country deserializeCountry(String countryString) throws JsonProcessingException {
        return objectMapper.readValue(countryString, Country.class);
    }

}
