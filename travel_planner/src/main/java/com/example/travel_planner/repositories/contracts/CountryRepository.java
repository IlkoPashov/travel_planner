package com.example.travel_planner.repositories.contracts;

import com.example.travel_planner.models.Country;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.List;

public interface CountryRepository {
    Country getCountryByName(String name) throws JsonProcessingException;

    List<String> getNeighborCountries(String countryName) throws JsonProcessingException;
    ;
    List<String> getNeighborCurrencyCodes(String countryName) throws JsonProcessingException;

}
