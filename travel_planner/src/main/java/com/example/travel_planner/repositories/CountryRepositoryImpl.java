package com.example.travel_planner.repositories;

import com.example.travel_planner.CacheWrapper;
import com.example.travel_planner.models.Country;
import com.example.travel_planner.models.CountrySerializer;
import com.example.travel_planner.repositories.contracts.CountryRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CountryRepositoryImpl implements CountryRepository {

    private final SessionFactory sessionFactory;

    private final CacheWrapper cache;

    private final CountrySerializer countrySerializer;


    @Autowired
    public CountryRepositoryImpl(SessionFactory sessionFactory, CacheWrapper cache, CountrySerializer countrySerializer) {
        this.sessionFactory = sessionFactory;
        this.cache = cache;
        this.countrySerializer = countrySerializer;
    }


    public Country getCountryByName(String name) throws JsonProcessingException {
        if (cache.keyExist(cache.COUNTRY_NAME_SUB_KEY.concat(name))) {
            Country country = countrySerializer.deserializeCountry((String) cache.getValue(cache.COUNTRY_NAME_SUB_KEY.concat(name)));
            return country;
        }

        String queryString = "SELECT c FROM Country c WHERE c.name = :name";
        try (Session session = sessionFactory.openSession()) {
            Query<Country> query = session.createQuery(queryString, Country.class);
            query.setParameter("name", name);
            List<Country> results = query.getResultList();
            if (results.isEmpty()) {
                return null;
            }
            Country country = results.get(0);
            cache.setValue(cache.COUNTRY_NAME_SUB_KEY.concat(name), countrySerializer.serializeCountry(country));
            return country;
        }
    }



    @Override
    public List<String> getNeighborCountries(String countryName) {

        try (Session session = sessionFactory.openSession()) {
            String hql = "SELECT n.neighbor.countryCode FROM CountryNeighbors n " +
                    "JOIN n.neighbor c " +
                    "JOIN n.country nc " +
                    "WHERE nc.name = :country";

            Query<String> query = session.createQuery(hql);
            query.setParameter("country", countryName);

            List<String> neighborCurrencyCodes = query.getResultList();
            return neighborCurrencyCodes;
        }
    }



    @Override
    public List<String> getNeighborCurrencyCodes(String countryName) {
        try (Session session = sessionFactory.openSession()) {
            String hql = "SELECT n.neighbor.currencyCode FROM CountryNeighbors n " +
                    "JOIN n.neighbor c " +
                    "JOIN n.country nc " +
                    "WHERE nc.name = :country";

            Query<String> query = session.createQuery(hql);
            query.setParameter("country", countryName);

            List<String> neighborCurrencyCodes = query.getResultList();
            return neighborCurrencyCodes;
        }
    }

}
