create table countries
(
    id            bigint unsigned auto_increment
        primary key,
    country       text null,
    country_code  text null,
    currency_code text null
);

create table country_neighbors
(
    id          bigint unsigned auto_increment
        primary key,
    country_id  bigint unsigned null,
    neighbor_id bigint unsigned null,
    constraint country_neighbors_ibfk_1
        foreign key (country_id) references countries (id),
    constraint country_neighbors_ibfk_2
        foreign key (neighbor_id) references countries (id)
);

create index country_neighbors_new_ibfk_1
    on country_neighbors (country_id);

create index country_neighbors_new_ibfk_2
    on country_neighbors (neighbor_id);

